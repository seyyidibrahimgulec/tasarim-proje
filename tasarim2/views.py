from django.http import HttpResponse
from django.shortcuts import render
import requests
from bs4 import BeautifulSoup
import serial
import serial.tools.list_ports
import sys
import time
import socket
import json
import wifi
from ipware import get_client_ip
import veri


def Postyontemi(request):
    client_address = request.META['REMOTE_ADDR']
    if(not client_address=="192.168.1.28"):
        print(client_address)
        veri.P.gun = veri.P.gun + 1

    return render(request, 'index.html', locals())


def ajax_kullanimi(reqest):
    hsoran = 1.35
    if (not veri.P.gun == 0):
        if (veri.P.gun % 2 == 1):
            data = {
                "1.genel": 6.85 * (veri.P.gun // 2 + 1),
                "1.genel+hs": 6.85 * hsoran * (veri.P.gun // 2),
                "1.mutfak": 6.22 * (veri.P.gun // 2 + 1),
                "1.mutfak+hs": 6.22 * hsoran * (veri.P.gun // 2),
                "1.tuvalet": 3.3 * (veri.P.gun // 2 + 1),
                "1.tuvalet+hs": 3.3 * hsoran * (veri.P.gun // 2),
                "1.salon": 3.4 * (veri.P.gun // 2 + 1),
                "1.salon+hs": 3.4 * hsoran * (veri.P.gun // 2),
                "1.daire": 19.77 * (veri.P.gun // 2 + 1),
                "1.daire+hs": 19.77 * hsoran * (veri.P.gun // 2),
                "2.genel": 7.5 * (veri.P.gun // 2 + 1),
                "2.genel+hs": 7.5 * hsoran * (veri.P.gun // 2),
                "2.mutfak": 7.06 * (veri.P.gun // 2 + 1),
                "2.mutfak+hs": 7.06 * hsoran * (veri.P.gun // 2),
                "2.tuvalet": 2.35 * (veri.P.gun // 2 + 1),
                "2.tuvalet+hs": 2.35 * hsoran * (veri.P.gun // 2),
                "2.salon": 0.9 * (veri.P.gun // 2 + 1),
                "2.salon+hs": 0.9 * hsoran * (veri.P.gun // 2),
                "2.daire": 17.01 * (veri.P.gun // 2 + 1),
                "2.daire+hs": 17.01 * hsoran * (veri.P.gun // 2)
            }
        else:
            data = {
                "1.genel": 6.85 * (veri.P.gun // 2),
                "1.genel+hs": 6.85 * hsoran * (veri.P.gun // 2),
                "1.mutfak": 6.22 * (veri.P.gun // 2),
                "1.mutfak+hs": 6.22 * hsoran * (veri.P.gun // 2),
                "1.tuvalet": 3.3 * (veri.P.gun // 2),
                "1.tuvalet+hs": 3.3 * hsoran * (veri.P.gun // 2),
                "1.salon": 3.4 * (veri.P.gun // 2),
                "1.salon+hs": 3.4 * hsoran * (veri.P.gun // 2),
                "1.daire": 19.77 * (veri.P.gun // 2),
                "1.daire+hs": 19.77 * hsoran * (veri.P.gun // 2),
                "2.genel": 7.5 * (veri.P.gun // 2),
                "2.genel+hs": 7.5 * hsoran * (veri.P.gun // 2),
                "2.mutfak": 7.06 * (veri.P.gun // 2),
                "2.mutfak+hs": 7.06 * hsoran * (veri.P.gun // 2),
                "2.tuvalet": 2.35 * (veri.P.gun // 2),
                "2.tuvalet+hs": 2.35 * hsoran * (veri.P.gun // 2),
                "2.salon": 0.9 * (veri.P.gun // 2),
                "2.salon+hs": 0.9 * hsoran * (veri.P.gun // 2),
                "2.daire": 17.01 * (veri.P.gun // 2),
                "2.daire+hs": 17.01 * hsoran * (veri.P.gun // 2)
            }

    else:
        data = {
            "1.genel": 0,
            "1.genel+hs": 0,
            "1.mutfak": 0,
            "1.mutfak+hs": 0,
            "1.tuvalet": 0,
            "1.tuvalet+hs": 0,
            "1.salon": 0,
            "1.salon+hs": 0,
            "1.daire": 0,
            "1.daire+hs": 0,
            "2.genel": 0,
            "2.genel+hs": 0,
            "2.mutfak": 0,
            "2.mutfak+hs": 0,
            "2.tuvalet": 0,
            "2.tuvalet+hs": 0,
            "2.salon": 0,
            "2.salon+hs": 0,
            "2.daire": 0,
            "2.daire+hs": 0
        }

    return HttpResponse(json.dumps(data))


def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func

    return decorate


@static_vars(count=0)
def foo():
    foo.count += 1
